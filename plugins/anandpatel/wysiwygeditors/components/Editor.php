<?php namespace AnandPatel\WysiwygEditors\Components;

use Cms\Classes\ComponentBase;
use AnandPatel\WysiwygEditors\Models\Settings;
use App;
use File;
use Request;
use Event;
use Tiipiik\Booking\Models\Room;

class Editor extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'tiipiik.booking::lang.components.room_list.name',
            'description' => 'tiipiik.booking::lang.components.room_list.description'
        ];
    }

    public function defineProperties()
    {
        return [
        ];
    }
    public function onRun()
    {
        $this->prepareVars();
    }

    public function prepareVars()
    {
        $this->page['name'] = $this->property('editorname');
        $roomid = $this->param('id');
        $room = Room::find($roomid);

        $this->page['value'] = $room->content_html;

        $this->page['width'] = (empty(Settings::instance()->editor_width)) ? '100%' : Settings::instance()->editor_width;
        $this->page['height'] = (empty(Settings::instance()->editor_height)) ? '500px' : Settings::instance()->editor_height;
        $this->page['lang'] = App::getLocale();

        $this->page['toolbar_tinymce'] = (empty(Settings::instance()->toolbar_tinymce)) ? "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media ocmediamanager" : Settings::instance()->toolbar_tinymce;

        $this->page['toolbar_ckeditor'] = (empty(Settings::instance()->toolbar_ckeditor)) ? "['Undo', 'Redo'], ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'], ['Format', 'FontSize'], ['ShowBlocks', 'SelectAll', 'RemoveFormat'], ['Source'], ['Maximize'], '/', ['Bold', 'Italic', 'Underline', 'Strike'], ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'], ['BulletedList', 'NumberedList', 'Outdent', 'Indent'], ['TextColor', 'BGColor'], ['Link', 'Unlink', 'Anchor'], ['Image', 'Table', 'oembed', 'SpecialChar', 'OcMediaManager', 'OcMediaVideo', 'OcMediaLink', 'OcMediaAudio']" : Settings::instance()->toolbar_ckeditor;

        $this->page['toolbar_froala_lg'] = (empty(Settings::instance()->toolbar_froala_lg)) ? "'undo', 'redo', 'clearFormatting', 'html', '|', 'bold', 'italic', 'underline', 'strikeThrough', '|', 'paragraphFormat', 'align', 'fontSize', 'color', '|', 'formatUL', 'formatOL', 'outdent', 'indent', '|', 'insertLink', 'insertImage', 'insertVideo', 'insertTable', '|', 'fullscreen'" : Settings::instance()->toolbar_froala_lg;

        $this->page['toolbar_froala_md'] = (empty(Settings::instance()->toolbar_froala_md)) ? "'undo', 'clearFormatting', 'html', '|', 'bold', 'italic', 'underline', '|', 'paragraphFormat', 'align', 'fontSize', 'color', '|', 'formatUL', 'formatOL', 'outdent', 'indent', '|', 'insertLink', 'insertImage', 'insertVideo', 'insertTable', '|', 'fullscreen'" : Settings::instance()->toolbar_froala_md;

        $this->page['toolbar_froala_sm'] = (empty(Settings::instance()->toolbar_froala_sm)) ? "'undo', 'html', '|', 'bold', 'italic', 'underline', '|', 'paragraphFormat', 'align', 'fontSize', 'color', '|', 'formatUL', 'formatOL', 'outdent', 'indent', '|', 'insertLink', 'insertImage', 'insertTable', '|', 'fullscreen'" : Settings::instance()->toolbar_froala_sm;

        $this->page['toolbar_froala_xs'] = (empty(Settings::instance()->toolbar_froala_xs)) ? "'undo', 'html', '|', 'bold', 'italic', '|', 'paragraphFormat', 'align', '|', 'formatUL', 'formatOL', 'outdent', 'indent', '|', 'insertLink', 'insertImage'" : Settings::instance()->toolbar_froala_xs;
    }


    // shamelessly copied from modules/backend/formwidgets/RicheEditor.php

    /**
     * Returns a single collection of available page links.
     * This implementation has room to place links under
     * different groups based on the link type.
     * @return array
     */
    public function onGetPageLinks()
    {
        $links = [];
        $types = $this->getPageLinkTypes();

        $links[] = ['name' => 'Select a page...', 'url' => false];

        $iterator = function($links, $level = 0) use (&$iterator) {
            $result = [];
            foreach ($links as $linkUrl => $link) {

                /*
                 * Remove scheme and host from URL
                 */
                $baseUrl = Request::getSchemeAndHttpHost();
                if (strpos($linkUrl, $baseUrl) === 0) {
                    $linkUrl = substr($linkUrl, strlen($baseUrl));
                }

                $linkName = str_repeat('&nbsp;', $level * 4);
                $linkName .= is_array($link) ? array_get($link, 'title', '') : $link;
                $result[] = ['name' => $linkName, 'url' => $linkUrl];

                if (is_array($link)) {
                    $result = array_merge(
                        $result,
                        $iterator(array_get($link, 'links', []), $level + 1)
                    );
                }
            }

            return $result;
        };

        foreach ($types as $typeCode => $typeName) {
            $links = array_merge($links, $iterator($this->getPageLinks($typeCode)));
        }

        return ['links' => $links];
    }

    /**
     * Returns a list of registered page link types.
     * This is reserved functionality for separating the links by type.
     * @return array Returns an array of registered page link types
     */
    protected function getPageLinkTypes()
    {
        $result = [];

        $apiResult = Event::fire('backend.richeditor.listTypes');
        if (is_array($apiResult)) {
            foreach ($apiResult as $typeList) {
                if (!is_array($typeList)) {
                    continue;
                }

                foreach ($typeList as $typeCode => $typeName) {
                    $result[$typeCode] = $typeName;
                }
            }
        }

        return $result;
    }

    protected function getPageLinks($type)
    {
        $result = [];
        $apiResult = Event::fire('backend.richeditor.getTypeInfo', [$type]);
        if (is_array($apiResult)) {
            foreach ($apiResult as $typeInfo) {
                if (!is_array($typeInfo)) {
                    continue;
                }

                foreach ($typeInfo as $name => $value) {
                    $result[$name] = $value;
                }
            }
        }

        return $result;
    }
}
