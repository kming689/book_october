<?php namespace Tiipiik\Booking\Models;

use App;
use Str;
use Model;
use Markdown;
use ValidationException;
use Tiipiik\Booking\Classes\TagProcessor;
use Cms\Classes\Controller as BaseController;
use Tiipiik\Booking\Models\Settings;
use Url;

/**
 * Room Model
 */
class AgencyStatus extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'agency_status';
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['*'];
    /*
        Avoid 'preview' to be passed on save & update requests
    */
    public $preview = null;


}
