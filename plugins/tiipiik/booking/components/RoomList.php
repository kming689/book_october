<?php namespace Tiipiik\Booking\Components;

use App;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Tiipiik\Booking\Models\Room;
use Tiipiik\Booking\Models\Settings;

use AuthM;
use Log;
use Tiipiik\Booking\Models\RoomManager;
use ICal\ICal;


class RoomList extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'tiipiik.booking::lang.components.room_list.name',
            'description' => 'tiipiik.booking::lang.components.room_list.description'
        ];
    }

    public function defineProperties()
    {
        return [
            'roomsPerPage' => [
                'title'             => 'tiipiik.booking::lang.components.room_list.params.rooms_per_page_title',
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'tiipiik.booking::lang.components.room_list.params.room_per_page_validation',
                'default'           => '10',
            ],
            'pageParam' => [
                'title'       => 'tiipiik.booking::lang.components.room_list.params.page_param_title',
                'description' => 'tiipiik.booking::lang.components.room_list.params.page_param_desc',
                'type'        => 'string',
                'default'     => ':page',
            ],
            'noRoomsMessage' => [
                'title'        => 'tiipiik.booking::lang.components.room_list.params.no_room_title',
                'description'  => 'tiipiik.booking::lang.components.room_list.params.no_room_desc',
                'type'         => 'string',
                'default'      => 'tiipiik.booking::lang.components.room_list.params.no_room_default'
            ],
        ];
    }

    public function getRoomPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRun()
    {
        $this->rooms = $this->page['rooms'] = $this->listRooms();
        $this->noRoomsMessage = $this->page['noRoomsMessage'] = $this->property('noRoomsMessage');
        // get the path of current theme
        //$this->themePath = $this->page['themePath'] = $this->themeUrl();

        //$this->roomParam = $this->page['roomParam'] = $this->property('roomParam');
        $this->roomPage = $this->page['roomPage'] = '/booking/room';//Settings::get('roomPage');
        $this->room_page_slug = $this->page['room_page_slug'] = Settings::get('room_page_slug');
    }

    /*
    protected function listRooms()
    {
        return Room::make()->listFrontEnd([
            'room' => Settings::get('room_page_slug'),
            'perPage' => $this->property('roomsPerPage'),
        ]);
    }*/

    protected function listRooms()
    {
        $user = AuthM::getUser();

        if (!$user) {
           return [];
        }

        $perPage = $this->property('roomsPerPage');

        $page =  $this->param('page')??0;

        Log::info($user->id);
        $relations = RoomManager::with(['room' => function($query){
          $query->with('featured_images','country','state','town');
        }])->with('role')->where('manager_id',$user->id)->paginate($perPage, $page)->toArray();


        foreach ($relations['data'] as $key => $value) {
          // code...

          $value['room']['role'] = $value['role']['name'];
          $relations['data'][$key] =  $value['room'];

        }

        Log::info('GGGGGGG',$relations);

        return $relations;
        // return Room::make()->listFrontEnd([
        //     'room' => Settings::get('room_page_slug'),
        //     'perPage' => $this->property('roomsPerPage'),
        //     'user_id' => $user->id,
        //     'page' => $this->param('page')
        // ]);
    }

    public function onLoadMoreRooms()
    {
        $data = post();

        $page = $data['page'];

        $perPage = $this->property('roomsPerPage');

        $start = $data['startdate'];
        $end = $data['enddate'];

        $rooms = RoomManager::with(['room' => function($query) use($start, $end){
          $query->with('featured_images','country','state','town')
                ->with(['events' => function($query) use($start, $end){
                      $query->where('date', '<', $end)->where('end_date', '>', $start);
                  }]);
        }])->where('manager_id',$data['user_id'])->paginate($perPage, $page)->toArray();

        foreach ($rooms['data'] as $key => $room) {
          // code...
          $this->loadAirbnbEvents($rooms['data'][$key]['room'], $start, $end);

        }

        return $rooms;

    }

    protected function loadAirbnbEvents(&$room, $start, $end)
    {
      $eventsAirbnb = [];
      if ($room['airbnb_ics'] && file_exists('/var/www/html/storage/temp/airbnb_'.$room['id'].'.ics')) {
        $ical = new ICal('/var/www/html/storage/temp/airbnb_'.$room['id'].'.ics', array(
          'defaultSpan'                 => 2,     // Default value
          'defaultTimeZone'             => 'UTC',
          'defaultWeekStart'            => 'MO',  // Default value
          'disableCharacterReplacement' => false, // Default value
          'filterDaysAfter'             => null,  // Default value
          'filterDaysBefore'            => null,  // Default value
          'replaceWindowsTimeZoneIds'   => false, // Default value
          'skipRecurrence'              => false, // Default value
          'useTimeZoneWithRRules'       => false, // Default value
        ));
       // Log::info("HHHHHHHHHHJJJJJJJJJ!!!!!!!!!");
       // Log::info($ical->eventCount);
       // Log::info($ical->freeBusyCount);

       //$eventsAirbnb = $ical->eventsFromRange('2018-07-01 12:00:00', '2018-08-01 17:00:00');
       $eventsAirbnb = $ical->eventsFromRange($start, $end);
      }
      foreach($eventsAirbnb as $key => $event) {
        // Log::info($event->dtstart_tz);
        // Log::info($event->uid);
        // Log::info($event->summary);
        // Log::info($event->location);
        // Log::info(date_create_from_format('Ymd',$event->dtend)->format('Y-m-d H:i:s'));
        // Log::info($event->description);
        // Log::info($event->attendee);
        // Log::info(date_create_from_format('Ymd',$event->dtstart)->format('Y-m-d H:i:s'));
        // Log::info("BBBBBBBBBBBBBBBBB");
        $room['events'][] = [
           'title' => $event->summary,
           'date' => date_create_from_format('Ymd',$event->dtstart)->format('Y-m-d H:i:s'),
           'end_date'   => date_create_from_format('Ymd',$event->dtend)->format('Y-m-d H:i:s'),
           'description' => $event->description,
           'allDay' => true,
           'backgroundColor' => '#F6380F',
           'borderColor'  => '#F6380F',
           'from' =>  'airbnb'
        ];
      }
    }
    public function onLoadMoreEventsOfRoom()
    {
      $data = post();
      $roomid = $data['roomid'];
      $start = $data['start'];
      $end = $data['end'];

      $room =  Room::where('id', $roomid)->with(['events' => function($query) use($start, $end){
            $query->where('date', '<', $end)->where('end_date', '>', $start);
        }])->first()->toArray();
      $this->loadAirbnbEvents($room, $start, $end);
      return $room;

    }


}
