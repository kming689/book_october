<?php namespace Tiipiik\Booking\Components;

use App;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Tiipiik\Booking\Models\Room;
use Tiipiik\Booking\Models\Settings;

use AuthM;
use Log;
use Tiipiik\Booking\Models\RoomManager;
use ICal\ICal;
use RainLab\User\Models\User;
use Tiipiik\Booking\Models\AgencyRelations;
use RainLab\User\Models\WeChatUser;
use Session;
use Tiipiik\Booking\Models\AgencyStatus;
use Weixin\Api as WeiXinApi;

class Agency extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'tiipiik.booking::lang.components.room_list.name',
            'description' => 'tiipiik.booking::lang.components.room_list.description'
        ];
    }

    public function defineProperties()
    {
        return [
        ];
    }

    public function onRun()
    {
      $roomid = $this->param('roomid');

      $agencyid = $this->param('id');

      $scene = $roomid.'-'.$agencyid;

      $this->page['qrcode_url'] = 'https://media.suubook.com/qrcode/'.$scene.'.png';
    }

    public function onCreateQRCode()
    {
      $roomid = $this->param('roomid');

      $agencyid = $this->param('id');

      $scene = $roomid.'-'.$agencyid;

      WeiXinApi::getWxAppcodeUnlimit($scene, 'pages/card/card', 'generate qrcode');

      $this->page['qrcode_url'] = 'https://media.suubook.com/qrcode/'.$scene.'.png';
    }

}
