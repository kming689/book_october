<?php namespace Tiipiik\Booking\Components;

use DB;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Request;
use Response;
use Tiipiik\Booking\Models\Room as RoomDetails;

use KurtJensen\MyCalendar\Models\Event;
use Log;
use Carbon\Carbon;
use Validator;
use ValidationException;
use Flash;
use AuthM;

use Storage;

use Weixin\Api as WeiXinApi;

use ICal\ICal;
use King\Helper\File as FileHelper;

use Input;

class Room extends ComponentBase
{
    protected $room;

    public function componentDetails()
    {
        return [
            'name'        => 'tiipiik.booking::lang.components.room.name',
            'description' => 'tiipiik.booking::lang.components.room.description'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'tiipiik.booking::lang.components.room.params.slug_title',
                'description' => 'tiipiik.booking::lang.components.room.params.slug_desc',
                'default'     => '{{ :slug }}',
                'type'        => 'string'
            ],
            'id' => [
                'title'       => 'tiipiik.booking::lang.components.room.params.slug_title',
                'description' => 'tiipiik.booking::lang.components.room.params.slug_desc',
                'default'     => '{{ :id }}',
                'type'        => 'number'
            ],
        ];
    }

    public function onRun()
    {
        $this->room = $this->page['room'] = $this->loadRoom();

        if ($this->room) {
          $exists = Storage::exists('qrcodeadmin/'.$this->room->id.'.png');

          if ($exists) {
            $this->page['qrcode_url'] = 'https://media.suubook.com/qrcode/'.$this->room->id.'.png';
          }
        }
        $this->page->meta_title =  $this->room?$this->room->name:'new room';
        $this->page->meta_description = $this->room?strip_tags($this->room->excerpt):'new room';

        // To use add `use DB` at the top of this page
        //echo '<pre>';
        //$queries = DB::getQueryLog();
        //dd($queries); // only last query -> dd(end($queries));

        //if (!$this->room) {
        //    return Response::make($this->controller->run('404'), 400, array());
        //}
    }

    protected function loadRoom()
    {
        $id = $this->property('id');
        return RoomDetails::where('id', '=', $id)->first();
    }

   public function onUpdateAirbnbEvents()
   {
        $id = $this->property('id');

        //FileHelper::downFile('https://www.airbnb.com/calendar/ical/26555683.ics?s=7b783c06eb56b8338c6e9c454a636207','/var/www/html/storage/temp','airbnb_'.$id.'.ics');
        $room = RoomDetails::find($id);
        if($room) {
           FileHelper::downFile($room->airbnb_ics, '/var/www/html/storage/temp','airbnb_'.$id.'.ics',1);
           //FileHelper::doGet('https://wxif.suubook.com/updateics?roomid='.$id);
           $room->airbnb_ics_update = 1;
           $room->save();
        }

        Flash::success('Save Success');

   }

   public function onUpdateCalendar(){
      $data = post();
      $rules = [
        'id' => 'required'
      ];
      $validation = Validator::make($data, $rules);
      if ($validation->fails()) {
         throw new ValidationException($validation);
      }

      $room = RoomDetails::find($data['id']);

      if ($room) {

        $room->airbnb_ics = $data['airbnb_ics'];

        $room->save();

      }

   }

    public function onUpdateRoom()
    {
         $data = post ();
         $rules = [
            'id' => 'required',
            'country_id' => 'required|min:1',
            'state_id'   => 'required:min:1',
            'roomname'       => 'required|min:1',
            'price'      => 'required'
         ];

         $validation = Validator::make($data, $rules);
         if ($validation->fails()) {
             throw new ValidationException($validation);
         }


         $room = RoomDetails::find($data['id']);

         $room->country_id = $data['country_id'];

         $room->state_id = $data['state_id'];

         $room->town_id  = ($data['town_id'] == '') ? 0: $data['town_id'];

         $room->name = $data['roomname'];

         $room->price = $data['price'];

         $room->is_available = ($data['published'] == 'on')?true:false;

         if (array_key_exists('editor1', $data)) {
           $room->content = $data['editor1'];
         }
         if (array_key_exists('longitude', $data)&&array_key_exists('latitude', $data)) {
           $room->longitude = $data['longitude'];
           $room->latitude = $data['latitude'];
         }

         $room->save();




         Flash::success('Save Success');

         return;
    }

    public function onCreateRoom()
    {
         $data = post ();
         $rules = [

            'country_id' => 'required|min:1',
            'state_id'   => 'required:min:1',
            'roomname'       => 'required|min:1',
            'maxpersons'  => 'required|min:1',
            'editor1'     => 'required',
            'price'      => 'required'
         ];

         $validation = Validator::make($data, $rules);
         if ($validation->fails()) {
             throw new ValidationException($validation);
         }
         Log::info($data['editor1']);
         Log::info('ddddddddgggvvvvvvvvvvvv');
         $user = AuthM::getUser();


         $room = new RoomDetails();

         $room->country_id = $data['country_id'] ?? 0;

         $room->state_id = $data['state_id'] ?? 0;

         $room->town_id  = ($data['town_id'] == '') ? 0 : $data['town_id'];

         $room->name = $data['roomname'];

         $room->max_persons = $data['maxpersons'];

         $room->is_available = ($data['published'] == 'on')?true:false;

         $room->content_html = $data['editor1'];

         $room->price = $data['price'];

         $room->save();

         $now = Carbon::now()->addHours(8);

         $room->managers()->attach($user->id,['created_at' => $now,'updated_at' => $now,'role_id' => 1]);




         Flash::success('Save Success');

         return;
    }

    public function onDeleteEvent()
    {
       $data = post ();
       $rules = [
          'id' => 'required|min:1'
       ];

       $validation = Validator::make($data, $rules);
       if ($validation->fails()) {
           throw new ValidationException($validation);
       }
       Log::info('lllllllllllllllllllll.......');
       Log::info($data['id']);
       Event::find($data['id'])->delete();

       return 'success';


    }

    public function onUploadImage()
    {
       $user = AuthM::getUser();

       $roomid = $this->property('id');

       if (Input::file('files')->isValid()) {
          $size = Input::file('files')->getSize();

          $imagePath = 'media/administrator/'.$user->id.'/'.$roomid.'/'.Input::file('files')->getClientOriginalName();

          Storage::put($imagePath , file_get_contents(Input::file('files')->getRealPath()));

          Log::info($imagePath);

          return 'https://media.suubook.com/'.$imagePath;
       }
       return '#';

    }

    public function onModifyEvent()
    {
      $data = post ();

      $rules = [
         'id' => 'required|min:1',
         'roomname'       => 'required|min:1',
         'editor1'    => 'required|min:1'
      ];
      Log::info('HHHHHHHHHHH', $data);

      $validation = Validator::make($data, $rules);
      if ($validation->fails()) {
          throw new ValidationException($validation);
      }
      $event = Event::find($data['id']);

      $event->name = $data['roomname'];
      $event->text = $data['editor1'];
      Log::info("BBBBBBBBBBbbbbbbb%%%%^^^^^^^^^");
      $event->is_published = array_key_exists('published',$data) ? ($data['published'] == 'on' ? true:false): false;
      $event->save();

      Flash::success('Successfully saved!');

      return 'date update success';
    }

    public  function onUpdateEvent()
    {
        $data = post ();

        $event = Event::find($data['event_id']);

        $start = Carbon::createFromTimestamp($data['start']);
        $end = Carbon::createFromTimestamp($data['end']);

        Log::info($data['start']);
        Log::info('HHHHHHHHHH');
        Log::info($data['allDay']);
        Log::info($data['end']);
        //Log::info($data['allDay']);
        $length = $data['end'] - $data['start'];

        $event->pattern  = substr($event->pattern,0,strpos($event->pattern,'UNTIL=')).'UNTIL='.$end->toDateString();

        $event->date = $start;
        $event->end_date = $end;
        $event->time = $start;
        $event->length = ($data['allDay'] == 'true') ? Carbon::createFromFormat('H:i:s',"00:00:00"):Carbon::createFromTimestamp($data['end'] - $data['start']);
        Log::info($event->end_date);
        Log::info('ggggggggggggggg');
        // try{
        $event->save();
       // }catch(\Exception $e){
       //    Log::info($e->getMessage());
       // }
        return 'date update success';
    }

    public  function onCreateEvent()
    {
        $data = post ();

        $event = new Event();

        $start = Carbon::createFromTimestamp($data['start']);
        $end = array_key_exists('end',$data)?Carbon::createFromTimestamp($data['end']):Carbon::createFromTimestamp($data['start'])->addDays(1);

        $event->room_id = $data['room_id'];
        $event->date = $start;
        $event->end_date = $end;
        $event->is_published = 1;
        $event->type = 1;
        $event->pattern  = 'FREQ=DAILY;INTERVAL=1;UNTIL='.$end->toDateString();
        $event->name = $data['name'];
        $event->length = ($data['allDay'] == 'true')? Carbon::createFromFormat('H:i:s','00:00:00'): Carbon::createFromTimestamp($data['end'] - $data['start']);
        $event->save();
        return  Response::json([
             'data'  => $event->id
         ]);
    }

    public function onMonthEvents()
    {
       $data = post ();
       $start = Carbon::createFromTimestamp($data['start'])->toDateString();
       $end = Carbon::createFromTimestamp($data['end'])->toDateString();
       Log::info('LLKKKKKKKK@@@@@@@@');
       Log::info($start);
       Log::info($end);
       $events = Event::where('room_id',$data['room_id'])->where(function($query) use($start,$end){
         $query->whereBetween('date',array($start,$end))
         ->orWhereBetween('end_date',array($start,$end));
       })->get()->toArray();
       Log::info('sssss',$events);


      $room =  RoomDetails::where('id', $data['room_id'])->first();

      $data = [];
      $eventsAirbnb = [];
      if ($room->airbnb_ics && file_exists('/var/www/html/storage/temp/airbnb_'.$room->id.'.ics')) {
        $ical = new ICal('/var/www/html/storage/temp/airbnb_'.$room->id.'.ics', array(
          'defaultSpan'                 => 2,     // Default value
          'defaultTimeZone'             => 'UTC',
          'defaultWeekStart'            => 'MO',  // Default value
          'disableCharacterReplacement' => false, // Default value
          'filterDaysAfter'             => null,  // Default value
          'filterDaysBefore'            => null,  // Default value
          'replaceWindowsTimeZoneIds'   => false, // Default value
          'skipRecurrence'              => false, // Default value
          'useTimeZoneWithRRules'       => false, // Default value
        ));
       Log::info("HHHHHHHHHHJJJJJJJJJ!!!!!!!!!");
       Log::info($ical->eventCount);
       Log::info($ical->freeBusyCount);

       //$eventsAirbnb = $ical->eventsFromRange('2018-07-01 12:00:00', '2018-08-01 17:00:00');
       $eventsAirbnb = $ical->eventsFromRange($start, $end);
     }
     foreach($eventsAirbnb as $key => $event) {
        // Log::info($event->dtstart_tz);
        // Log::info($event->uid);
        // Log::info($event->summary);
        // Log::info($event->location);
        // Log::info(date_create_from_format('Ymd',$event->dtend)->format('Y-m-d H:i:s'));
        // Log::info($event->description);
        // Log::info($event->attendee);
        // Log::info(date_create_from_format('Ymd',$event->dtstart)->format('Y-m-d H:i:s'));
        // Log::info("BBBBBBBBBBBBBBBBB");
        $data[] = [
           'title' => $event->summary,
           'start' => date_create_from_format('Ymd',$event->dtstart)->format('Y-m-d H:i:s'),
           'end'   => date_create_from_format('Ymd',$event->dtend)->format('Y-m-d H:i:s'),
           'description' => $event->description,
           'allDay' => true,
           'backgroundColor' => '#F6380F',
           'borderColor'  => '#F6380F',
           'from' =>  'airbnb'
        ];
     }
       Log::info("!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
      foreach ($events as $key => $event) {
        # code...
        Log::info($event['date'].' '.$event['time']);
        Log::info($event['length']);
        $data[] = [
            'id'    => $event['id'],
            'title' => $event['name'],
            'start' => $event['date'],
            'end'   => $event['end_date'],
            'allDay' => $event['length'] === '00:00:00' ? true:false,
            'description' => '',
            'backgroundColor' => '#00c0ef',
            'borderColor'    => '#00c0ef',
            'from'   => 'ldeer'
        ];
      }

       return  Response::json([
            'data'  => $data
        ]);
    }

    public function onCreateQRCode()
    {
      $roomid = $this->property('id');

      WeiXinApi::getWxAppcodeUnlimit($roomid, 'pages/card/card', 'generate qrcode');

      $this->page['qrcode_url'] = 'https://media.suubook.com/qrcode/'.$roomid.'.png';
    }
}
