<?php namespace Tiipiik\Booking\Components;

use App;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Tiipiik\Booking\Models\Room;
use Tiipiik\Booking\Models\Settings;

use AuthM;
use Log;
use Tiipiik\Booking\Models\RoomManager;
use ICal\ICal;
use RainLab\User\Models\User;
use Tiipiik\Booking\Models\AgencyRelations;
use RainLab\User\Models\WeChatUser;
use Session;
use Tiipiik\Booking\Models\AgencyStatus;

class AgencyList extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'tiipiik.booking::lang.components.room_list.name',
            'description' => 'tiipiik.booking::lang.components.room_list.description'
        ];
    }

    public function defineProperties()
    {
        return [
            'roomsPerPage' => [
                'title'             => 'tiipiik.booking::lang.components.room_list.params.rooms_per_page_title',
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'tiipiik.booking::lang.components.room_list.params.room_per_page_validation',
                'default'           => '10',
            ],
            'pageParam' => [
                'title'       => 'tiipiik.booking::lang.components.room_list.params.page_param_title',
                'description' => 'tiipiik.booking::lang.components.room_list.params.page_param_desc',
                'type'        => 'string',
                'default'     => ':page',
            ],
            'noRoomsMessage' => [
                'title'        => 'tiipiik.booking::lang.components.room_list.params.no_room_title',
                'description'  => 'tiipiik.booking::lang.components.room_list.params.no_room_desc',
                'type'         => 'string',
                'default'      => 'tiipiik.booking::lang.components.room_list.params.no_room_default'
            ],
        ];
    }

    public function getRoomPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    protected function role()
    {
      $manager = AuthM::getUser();

      $roomid = $this->param('roomid');

      $relation = RoomManager::with('role')->where('room_id',$roomid)->where('manager_id',$manager->id)->first();

      return $relation->role->name;
    }

    public function onRun()
    {
        $this->agencys = $this->page['agencys'] = $this->listAgencys();
        $this->noAgencysMessage = $this->page['noAgencysMessage'] = $this->property('noAgencysMessage');
        $this->role = $this->page['role'] = $this->role();
        $this->searchs = $this->page['searchs'] = $this->getSearchField();
        $this->roomid = $this->page['roomid'] = $this->param('roomid');
        $statuses = AgencyStatus::all()->toArray();
        $statusobj = [];
        foreach($statuses as $key => $value){
           $statusobj[$value['id']] = $value;
        }
        $this->page['statuses'] = $statusobj;
        // get the path of current theme
        //$this->themePath = $this->page['themePath'] = $this->themeUrl();

        //$this->roomParam = $this->page['roomParam'] = $this->property('roomParam');
        $this->agencyPage = $this->page['agencyPage'] = '/booking/agency';//Settings::get('roomPage');
        //$this->room_page_slug = $this->page['room_page_slug'] = Settings::get('room_page_slug');
    }

    /*
    protected function listRooms()
    {
        return Room::make()->listFrontEnd([
            'room' => Settings::get('room_page_slug'),
            'perPage' => $this->property('roomsPerPage'),
        ]);
    }*/

    protected function _listAgencys()
    {
        $user = AuthM::getUser();

        $perPage = $this->property('agencysPerPage');

        $page =  $this->param('page')??0;

        $roomid = $this->param('roomid');


        $relations = AgencyRelations::where('room_id',$roomid)->with(['user' => function($query){
          $query->with('avatar')->with('wechatuser');
        }])->with('status')->paginate($perPage, $page)->toArray();

        return $relations;
        // return Room::make()->listFrontEnd([
        //     'room' => Settings::get('room_page_slug'),
        //     'perPage' => $this->property('roomsPerPage'),
        //     'user_id' => $user->id,
        //     'page' => $this->param('page')
        // ]);
    }

    public function onSearchField()
    {
      $data = post();
      $fields = [
        'name',
        'wxname',
        'status'
      ];
      Log::info('GGGGGGGGGGGGGGVVVV',$data);

      foreach ($fields as $field) {
        // code...
        if (array_key_exists($field, $data)&&$data[$field]) {
           Session::put('agencylist_search_'.$field, $data[$field]);
        } else {
           Session::forget('agencylist_search_'.$field);
        }
      }
    }

    public function getSearchField()
    {
      $fields = [
        'name',
        'wxname',
        'status'
      ];

      $data = [];

      foreach ($fields as $key) {
        // code...
        $data[$key] = Session::get('agencylist_search_'.$key,'default');
      }

      Log::info('nnnnnnnsss',$data);
      Log::info('nnnnnnnnnnnnnnnnnnnnnn');

      return $data;
    }

    protected function listAgencys()
    {
        $manager = AuthM::getUser();

        $page =  $this->param('page');

        $roomid = $this->param('roomid');

        $perPage = $this->property('agencysPerPage');



        $rules = [
          'name'   => function($query,$value){
             return $query->where('name',$value);
          },
          'wxname' => function($query,$value){
            return $query->whereHas('wechatuser', function ($query) use($value) {
                $query->where('nickname', 'like', '%'.$value.'%');
            });
          },
          'status' => function($query, $value){
            return $query->wherePivot('status_id',$value);
          }
        ];

        $query =  Room::find($roomid)->users()->withPivot('status_id')->with('wechatuser');



        foreach ($rules as $key => $field) {
          // code...
            $value =  Session::get('agencylist_search_'.$key,'default');
            if ($value != 'default')
               $query = $field($query,$value);
        }

        $agencys = $query->paginate($perPage, $page)->toArray();

        return $agencys;
    }



}
