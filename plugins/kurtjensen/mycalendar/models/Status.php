<?php namespace KurtJensen\MyCalendar\Models;

use Form;
use Lang;
use Model;
use System\Classes\PluginManager;

/**
 * Category Model
 */
class Status extends Model {
	use \October\Rain\Database\Traits\Validation;

	public $permOptions = [];

	public $table = 'event_status';

	/*
		     * Validation

		    name
		    slug
		    description
	*/
	public $rules = [
	];

	protected $guarded = [];



	/**
	 * @var array Cache for nameList() method
	 */
	protected static $nameList = [];
}
