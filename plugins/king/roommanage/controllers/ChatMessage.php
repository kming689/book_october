<?php namespace King\RoomManage\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use King\RoomManage\Models\Message as MessageModel;
use Pusher\Pusher;
use Log;

class ChatMessage extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    //public $requiredPermissions = ['king.market.access_symbols'];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('King.RoomManage', 'RoomManage', 'RoomManage');
    }

    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $categoryId) {
                if ((!$category = Symbol::find($categoryId)))
                    continue;

                $category->delete();
            }

            Flash::success('Successfully deleted those categories.');
        }

        return $this->listRefresh();
    }

    public function onPushMessage($recordId)
    {
        $message = $this->formFindModelObject($recordId);

        $options = array(
          'cluster' => 'ap1',
          'encrypted' => true
        );
        $pusher = new Pusher(
          '426955762f302757a04c',
          'cafe7f8a43e98a4e3c99',
          '560588',
          $options
        );
        // Log::info($message->manager->channel);
        // $result = $pusher->get_channels();
        // Log::info(count($result->channels));
        // Log::info('gggggg',$result->channels);
        // foreach ($result->channels as $key => $channel) {
        //   // code...
        //    Log::info($key);
        // }
        $channeldata = $pusher->get('/channels/presence-'.$message->manager->channel.'/users');
        // Log::info('lllllllll',$prechannel);
        // $channeldata = json_decode($prechannel,true);


        foreach ($channeldata['result']['users'] as $key => $user) {
          // code...
               if ( ($message->direction==1) && ($user['id']  == $message->manager->id) ) {
                 $data['content'] = $message->content;
                 $data['manager'] =['id' => $message->manager->id,'name' => $message->manager->name,'avatar' => $message->manager->icon?$message->manager->icon->path:null];
                 $data['user'] = ['id' => $message->user->id,'name' => $message->user->name,'avatar' => $message->user->avatar?$message->user->avatar->path:null];
                 $data['published_time'] = $message->created_at->toDateTimeString();
                 $data['direction'] = $message->direction;
                 $pusher->trigger('presence-'.$message->manager->channel, 'chat', $data);
                 $message->real = 1;
                 $message->save();
                 break;
               }
        }

        return;



    }
}
