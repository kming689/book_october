<?php namespace King\RoomManage\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use King\RoomManage\Models\Message as MessageModel;
use Pusher\Pusher;
use Log;

class Message extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    //public $requiredPermissions = ['king.market.access_symbols'];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('King.RoomManage', 'RoomManage', 'RoomManage');
    }

    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $categoryId) {
                if ((!$category = Symbol::find($categoryId)))
                    continue;

                $category->delete();
            }

            Flash::success('Successfully deleted those categories.');
        }

        return $this->listRefresh();
    }

    public function onPushMessage($recordId)
    {
        //$message = MessageModel::find($recordId);
        $message = $this->formFindModelObject($recordId);

        $options = array(
          'cluster' => 'ap1',
          'encrypted' => true
        );
        $pusher = new Pusher(
          '426955762f302757a04c',
          'cafe7f8a43e98a4e3c99',
          '560588',
          $options
        );
        Log::info($message->content);
        Log::info($message->event);
        Log::info($message->channel);

        $data['title'] = $message->title;
        $data['url'] = $message->url??'#';
        $data['img'] = $message->icon?$message->icon->path:'none';
        $data['from'] = $message->from;
        $data['created_at'] = $message->created_at->toDateTimeString();
        $type = $message->type;
        $data['class'] = (($type->name=='notification')||($type->name == 'task'))?$message->classcss:'';
        $data['percent'] = $type->name=='task' ? $message->percent:'';
        $pusher->trigger($message->channel, $message->event, $data);

    }
}
