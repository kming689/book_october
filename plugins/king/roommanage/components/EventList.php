<?php namespace King\RoomManage\Components;

use App;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Tiipiik\Booking\Models\Room;
use Tiipiik\Booking\Models\Settings;
use AuthM;
use Log;
use Flash;
use Tiipiik\Booking\Models\RoomManager;
use Session;

use KurtJensen\MyCalendar\Models\Event;


class EventList extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'dashboard',
            'description' => 'event'
        ];
    }

    public function defineProperties()
    {
        return [
        ];
    }

    public function onRun()
    {
        $this->events = $this->page['events'] = $this->listEvents();
        $this->role =$this->page['role'] = $this->role();
        $this->room = $this->page['room'] = $this->room();
        $this->searchs = $this->page['searchs'] = $this->getSearchField();
    }

    protected function room()
    {
      return $this->param('room');
    }

    protected function role()
    {
      $manager = AuthM::getUser();

      $room = $this->param('room');

      $relation = RoomManager::with('role')->where('room_id',$room)->where('manager_id',$manager->id)->first();

      return $relation->role->name;
    }

    public function getSearchField()
    {
      $fields = [
        'status',
        'instart',
        'inend',
        'outstart',
        'outend',
        'money',
        'name'
      ];

      $data = [];

      foreach ($fields as $key) {
        // code...
        $data[$key] = Session::get('eventlist_search_'.$key,'default');
      }

      Log::info('nnnnnnnsss',$data);
      Log::info('nnnnnnnnnnnnnnnnnnnnnn');

      return $data;
    }

    public function onSearchField()
    {
      $data = post();
      $fields = [
        'status',
        'instart',
        'inend',
        'outstart',
        'outend',
        'money',
        'name'
      ];
      Log::info('GGGGGGGGGGGGGGVVVV',$data);

      foreach ($fields as $field) {
        // code...
        if (array_key_exists($field, $data)&&$data[$field]) {
           Session::put('eventlist_search_'.$field, $data[$field]);
        } else {
           Session::forget('eventlist_search_'.$field);
        }
      }
    }

    protected function listEvents()
    {
        $manager = AuthM::getUser();

        $page =  $this->param('page');

        $room = $this->param('room');

        $perPage = $this->property('eventsPerPage');

        $rules = [
          'status' => function($query,$value){
            return $query->where('status',$value);
          },
          'instart' => function($query,$value){
            return $query->where('date',">",$value);
          },
          'inend'  => function($query,$value){
             return $query->where('date',"<",$value);
          },
          'outstart' => function($query,$value){
            return $query->where('date',">",$value);
          },
          'outend'  => function($query,$value){
             return $query->where('date',"<",$value);
          },
          'money'   => function($query,$value){
             return $query->where('money',">",$value);
          },
          'name'   => function($query,$value){
             return $query->where('name',$value);
          }
        ];

        $query = Event::with(['user' => function($query){
                  $query->with('avatar');
                }])->with('status')->where('room_id',$room);

        foreach ($rules as $key => $field) {
          // code...
            $value =  Session::get('eventlist_search_'.$key,'default');
            if ($value != 'default')
               $query = $field($query,$value);
        }

        // $events = Event::with(['user' => function($query){
        //           $query->with('avatar');
        //         }])->with('status')->where('room_id',$room)->paginate($perPage, $page)->toArray();
        $events = $query->paginate($perPage, $page)->toArray();

        return $events;

    }


}
