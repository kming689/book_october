<?php namespace King\RoomManage\Components;

use App;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Tiipiik\Booking\Models\Room;
use Tiipiik\Booking\Models\Settings;
use AuthM;
use Log;
use Flash;
use Tiipiik\Booking\Models\RoomManager;
use Session;
use Carbon\Carbon;

use Pusher\Pusher;

use King\RoomManage\Models\Message;

use King\RoomManage\Models\MessageStatus;

use King\RoomManage\Models\ChatStatus;
use King\RoomManage\Models\ChatMessage;


class PusherAuth extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'dashboard',
            'description' => 'message'
        ];
    }

    public function defineProperties()
    {
        return [
        ];
    }

    public function onRun()
    {
        $this->authstring = $this->page['authstring'] = $this->auth();
    }

    public function auth()
    {
      $data = post();
      $manager = AuthM::getUser();
      Log::info('sssss',$data);
      Log::info($manager->id);
      $pusher = new Pusher('426955762f302757a04c', 'cafe7f8a43e98a4e3c99', '560588');
      $presence_data = array('name' => $manager->name);

      $res = $pusher->presence_auth($data['channel_name'], $data['socket_id'], $manager->id, $presence_data);
      Log::info($res);
      return $res;
      //$pusher->presence_auth($data['channel_name'], $data['socket_id'], $manager->id, $presence_data);
    }

}
