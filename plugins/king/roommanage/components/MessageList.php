<?php namespace King\RoomManage\Components;

use App;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Tiipiik\Booking\Models\Room;
use Tiipiik\Booking\Models\Settings;
use AuthM;
use Log;
use Flash;
use Tiipiik\Booking\Models\RoomManager;
use Session;

use King\RoomManage\Models\Message;

use King\RoomManage\Models\MessageStatus;


class MessageList extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'dashboard',
            'description' => 'message'
        ];
    }

    public function defineProperties()
    {
        return [
        ];
    }

    public function onRun()
    {
        $this->allmessages = $this->page['allmessages'] = $this->listMessages();
        $this->searchs =  $this->page['searchs']  = $this->getSearchField();
        $this->statuss =  $this->page['statuss']  = MessageStatus::all()->toArray();
    }

    protected function room()
    {
      return $this->param('room');
    }

    protected function role()
    {
      $manager = AuthM::getUser();

      $room = $this->param('room');

      $relation = RoomManager::with('role')->where('room_id',$room)->where('manager_id',$manager->id)->first();

      return $relation->role->name;
    }

    public function getSearchField()
    {
      $fields = [
        'title',
        'status',
        'start',
        'end'
      ];

      $data = [];

      foreach ($fields as $key) {
        // code...
        $data[$key] = Session::get('messagelist_search_'.$key,'default');
      }

      return $data;
    }

    public function onSearchField()
    {
      $data = post();
      $fields = [
        'title',
        'status',
        'start',
        'end'
      ];
      Log::info('GGGGGGGGGGGGGGVVVV',$data);

      foreach ($fields as $field) {
        // code...
        if (array_key_exists($field, $data)&&$data[$field]) {
           Session::put('messagelist_search_'.$field, $data[$field]);
        } else {
           Session::forget('messagelist_search_'.$field);
        }
      }
    }

    protected function listMessages()
    {
        $manager = AuthM::getUser();

        $page =  $this->param('page');

        $perPage = $this->property('messagesPerPage');

        $rules = [
          'title' => function($query,$value){
            return $query->where('title',$value);
          },
          'status' => function($query,$value){
            return $query->where('status_id',$value);
          },
          'start' => function($query,$value){
            return $query->where('created_at',">",$value);
          },
          'end' => function($query,$value){
            return $query->where('created_at',"<",$value);
          }
        ];

        $query = Message::with('status','type');

        foreach ($rules as $key => $field) {
          // code...
            $value =  Session::get('messagelist_search_'.$key,'default');
            if ($value != 'default')
               $query = $field($query,$value);
        }

        // $events = Event::with(['user' => function($query){
        //           $query->with('avatar');
        //         }])->with('status')->where('room_id',$room)->paginate($perPage, $page)->toArray();
        $events = $query->paginate($perPage, $page)->toArray();

        return $events;

    }


}
