<?php namespace King\RoomManage\Components;

use App;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Tiipiik\Booking\Models\Room;
use Tiipiik\Booking\Models\Settings;
use AuthM;
use Log;
use Flash;
use Tiipiik\Booking\Models\RoomManager;
use Session;
use Carbon\Carbon;


use King\RoomManage\Models\Message;

use King\RoomManage\Models\MessageStatus;

use King\RoomManage\Models\ChatStatus;
use King\RoomManage\Models\ChatMessage as ChatMessageModel;


class ChatMessage extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'dashboard',
            'description' => 'message'
        ];
    }

    public function defineProperties()
    {
        return [
        ];
    }

    public function onRun()
    {
        $manager = AuthM::getUser();
        if ($manager) {
          $this->chatdatas = $this->page['chatdatas'] = $this->listChatDatas($manager);
          $this->chatcount = $this->page['chatcount'] = $this->chatCount($manager);
        }
    }
    protected function chatCount($manager)
    {
       return ChatStatus::with(['user' => function($query){
            $query->with('avatar');
       }])->where('manager_id',$manager->id)->where('appended','>',0)->sum('appended');
    }
    protected function listChatDatas($manager)
    {
        $chats = ChatStatus::with(['user' => function($query){
             $query->with('avatar');
        }])->where('manager_id',$manager->id)->where('appended','>',0)->orderBy('updated_at','desc')->take(8)->get();

        return $chats;

    }

    public function onChats()
    {
      $data = post();
      $manager = AuthM::getUser();

      $chat = ChatStatus::where('user_id',$data['user_id'])->where('manager_id',$manager->id)->first();

      if ($chat) {

        $chat->updated_at = Carbon::now()->addHours(8);

        ChatMessageModel::where('user_id',$data['user_id'])->where('manager_id',$manager->id)
        ->where('direction',1)->where('created_at','>',$chat->received_time->toDateTimeString())->get();

        $chat->appended = 0;

        $chat->save();




      }


    }


}
