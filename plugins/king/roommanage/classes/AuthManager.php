<?php namespace King\RoomManage\Classes;

use October\Rain\Auth\Manager as RainAuthManager;
// use RainLab\User\Models\Settings as UserSettings;
// use RainLab\User\Models\UserGroup as UserGroupModel;

class AuthManager extends RainAuthManager
{
    protected static $instance;

    protected $sessionKey = 'manager_auth';

    protected $userModel = 'King\RoomManage\Models\Manager';

    protected $throttleModel = 'King\RoomManage\Models\Throttle';

    //protected $groupModel = 'RainLab\User\Models\UserGroup';

    //protected $throttleModel = 'RainLab\User\Models\Throttle';

    public function init()
    {
        // $this->useThrottle = UserSettings::get('use_throttle', $this->useThrottle);
        // $this->requireActivation = UserSettings::get('require_activation', $this->requireActivation);
        parent::init();
    }

    /**
     * {@inheritDoc}
     */
    public function extendUserQuery($query)
    {
        $query->withTrashed();
    }

    /**
     * {@inheritDoc}
     */
    public function register(array $credentials, $activate = false, $autoLogin = true)
    {
        return parent::register($credentials, $activate, true);
    }
}
