<?php namespace King\RoomManage\Classes;

use AuthM;
use Closure;
use Response;

class AuthMiddleware
{
    public function handle($request, Closure $next)
    {
        if (!AuthM::check()) {
            return Response::make('Forbidden', 403);
        }

        return $next($request);
    }
}
