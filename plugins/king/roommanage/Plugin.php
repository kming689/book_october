<?php namespace King\RoomManage;

use System\Classes\PluginBase;
use Backend;

use Illuminate\Foundation\AliasLoader;

use Event;

use Log;

use Flash;

use App;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
      return [
          \King\RoomManage\Components\Session::class       => 'sessionm',
          \King\RoomManage\Components\Account::class       => 'accountm',
          \King\RoomManage\Components\ResetPassword::class => 'resetPasswordm',
          \King\RoomManage\Components\EventList::class     => 'eventList',
          \King\RoomManage\Components\MessageList::class     => 'messageList',
          \King\RoomManage\Components\ChatMessage::class   => 'chatMessage',
          \King\RoomManage\Components\PusherAuth::class    => 'pusherAuth'
      ];
    }

    public function registerSettings()
    {
    }

        /**
     * {@inheritdoc}
     */
    public function registerPermissions(): array
    {
        return [
            'king.roommanage.access_comments' => [
                'tab' => 'king.roommanage::lang.plugin.name',
                'label' => 'king.roommanage::lang.permissions.access_comments'
            ]
        ];
    }

    public function registerNavigation()
    {
    	return [
                'manager' => [
                    'label' => '酒店管理员',
                    'url'   => Backend::url('king/roommanage/manager'),
                    'icon'        => 'icon-pencil',
                    'iconSvg'     => 'plugins/king/market/assets/images/article.svg',
                    'permissions' => ['king.roommanage.*'],
                    'order'       => 50
                ],
                'message' => [
                   'label' => '消息管理',
                   'url'   => Backend::url('king/roommanage/message'),
                   'icon'        => 'icon-pencil',
                   'iconSvg'     => 'plugins/king/market/assets/images/article.svg',
                   'permissions' => ['king.roommanage.*'],
                   'order'       => 50
                ],
                'chat' => [
                   'label' => '实时消息管理',
                   'url'  => Backend::url('king/roommanage/chatmessage'),
                   'icon'        => 'icon-pencil',
                   'iconSvg'     => 'plugins/king/market/assets/images/article.svg',
                   'permissions' => ['king.roommanage.*'],
                   'order'       => 50
                ]
    	];
    }

        /**
     * Inject into Blog Posts
     */
    public function boot()
    {
        // Extend the controller

    }

    public function register()
    {
        $alias = AliasLoader::getInstance();
        $alias->alias('AuthM', 'King\RoomManage\Facades\Auth');

        App::singleton('manager.auth', function() {
            return \King\RoomManage\Classes\AuthManager::instance();
        });
    }
}
