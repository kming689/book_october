<?php namespace King\RoomManage\Models;

use Model;

class ChatStatus extends Model
{
    /**
     * @var string The database table used by the model.
     */
    protected $table = 'chat_status';

    /**
     * @var array Relations
     */

    public $belongsTo = [
        'user'   =>  'RainLab\User\Models\User',
        'manager' => 'King\RoomManage\Models\Manager'
    ];
}
