<?php namespace King\RoomManage\Models;

use October\Rain\Auth\Models\Throttle as ThrottleBase;

class Throttle extends ThrottleBase
{
    /**
     * @var string The database table used by the model.
     */
    protected $table = 'manager_throttle';

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'manager' => Manager::class
    ];
}
