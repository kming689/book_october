<?php namespace King\RoomManage\Models;

use Model;

class Message extends Model
{
    /**
     * @var string The database table used by the model.
     */
    protected $table = 'manager_messages';

    /**
     * @var array Relations
     */

    public $belongsTo = [
        'status' => 'King\RoomManage\Models\MessageStatus',
        'type'   => 'King\RoomManage\Models\MessageType',
        'user'   =>  'RainLab\User\Models\User'
    ];
    public $attachOne = [
        'icon' => \System\Models\File::class,
    ];
}
