<?php namespace King\DashBoard;

use System\Classes\PluginBase;
use Backend;

use Illuminate\Foundation\AliasLoader;

use Event;

use Log;

use Flash;

use App;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
      return [
          \King\RoomManage\Components\Session::class       => 'sessionm',
          \King\RoomManage\Components\Account::class       => 'accountm',
          \King\RoomManage\Components\ResetPassword::class => 'resetPasswordm'
      ];
    }

    public function registerSettings()
    {
    }

        /**
     * {@inheritdoc}
     */
    public function registerPermissions(): array
    {
    }

    public function registerNavigation()
    {
    }

        /**
     * Inject into Blog Posts
     */
    public function boot()
    {
        // Extend the controller

    }
}
