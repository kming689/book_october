<?php namespace King\DashBoard\Components;

use App;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Tiipiik\Booking\Models\Room;
use Tiipiik\Booking\Models\Settings;
use AuthM;
use Log;
use Tiipiik\Booking\Models\RoomManager;

class EventInfo extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'dashboard',
            'description' => 'event'
        ];
    }

    public function defineProperties()
    {
        return [
        ];
    }

    public function onRun()
    {
        $this->evcount = $this->page['evcount'] = $this->evcount();
    }

    protected function evcount()
    {

    }


}
