<?php namespace RainLab\User\Models;

use Model;
/**
 * User Group Model
 */
class WechatUser extends Model
{

    /**
     * @var string The database table used by the model.
     */
    protected $table = 'wechat_users';

    public $belongsTo = [
        'user' => ['RainLab\User\Models\User', 'delete' => true],
    ];
}
