<?php namespace RainLab\Location\Components;

use Auth;
use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use KurtJensen\MyCalendar\Models\Event as MyEvents;
use KurtJensen\MyCalendar\Models\Settings;
use Lang;

class LocationSelect extends ComponentBase
{
    public $usePermissions = 0;
    public $user = null;
    public $calEvent = null;

    public function componentDetails()
    {
        return [
            'name' => 'location select',
            'description' => 'location select',
        ];
    }

    public function defineProperties()
    {
        return [
        ];
    }

    public function onInit()
    {

    }
    public function onRun()
    {

    }
}
