<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Weixin;
use Cache;
use Log;
use DB;
//use App\Modules\Qiniu\Functions as QiniuFuncs;
use Storage;

class Api {

    /*发送get请求*/
    public static function geturl($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        $data = curl_exec($ch);
        $response = json_decode($data,TRUE);
        return $response;
    }

    /*发送post请求*/
    public static  function posturl($arr,$url){
        //$postJosnData = json_encode($arr);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $arr);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        $data = curl_exec($ch);
        return $data;
    }

    /*获取微信用户code*/
    public static function usercode($curl){
        $redirect_uri = urlencode ($curl);
        $appid=config('database.weixin.appid');
        $url ="https://open.weixin.qq.com/connect/oauth2/authorize?appid=$appid&redirect_uri=$redirect_uri&response_type=code&scope=snsapi_base&state=1#wechat_redirect";
        header("Location:".$url);
    }

    /*获取用户openid*/
    public static function useropenid($code){
        $appid=config('database.weixin.appid');
        $secret=config('database.weixin.secret');
        $url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=$appid&secret=$secret&code=$code&grant_type=authorization_code";
        $oauth2 =Api::geturl($url);
        $openid=$oauth2['openid'];
        return $openid;
    }

      /*微信认证url*/
    public static function index($nonce,$timestamp,$token,$signature,$echostr){
        //参数字典序排序
        $array = array();
        $array = array($nonce, $timestamp, $token);
        sort($array);
        //验证
        $str = sha1( implode( $array ) );//sha1加密
        //对比验证处理好的str与signature,若确认此次GET请求来自微信服务器，请原样返回echostr参数内容，则接入生效，成为开发者成功，否则接入失败。
        if( $str  == $signature ){
          return $echostr;
        }
        else{
            //接入成功后的其他处理
           return false;
        }
    }

    /*随机数*/
     public static function randCode($length = 16, $type = 1){
        $arr = array(1 => "0123456789", 2 => "abcdefghijklmnopqrstuvwxyz", 3 => "ABCDEFGHIJKLMNOPQRSTUVWXYZ", 4 => "~@#$%^&*(){}[]|");
        if ($type == 0) {
        array_pop($arr);
        $string = implode("", $arr);
        } elseif ($type == "-1") {
        $string = implode("", $arr);
        } else {
        $string = $arr[$type];
        }
        $count = strlen($string) - 1;
        $code = '';
        for ($i = 0; $i < $length; $i++) {
        $code .= $string[rand(0, $count)];
        }
        return $code;
    }

    /*当前时间毫秒*/
     public static function gettime(){
            $time = explode (" ", microtime () );
            $time = $time [1] . ($time [0] * 1000);
            $time2 = explode ( ".", $time );
            $time = $time2 [0];
            return $time;
    }

   /*数组转换为xml*/
   public static function ToXml($input){
    	$xml = "<xml>";
    	foreach ($input as $key=>$val)
    	{
    		if (is_numeric($val)){
    			$xml.="<".$key.">".$val."</".$key.">";
    		}else{
    			$xml.="<".$key."><![CDATA[".$val."]]></".$key.">";
    		}
        }
        $xml.="</xml>";
        return $xml;
   }

   /*获取access_token个数100000个*/
   public static function token(){
       //Cache::forget('access_token');
      if(Cache::has("access_token")){
          $arr = Cache::get('access_token');
      }
      else{
           $AppId = config('weixin.app.appid');
           $AppSecret = config('weixin.app.appsecret');
           $getUrl = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.$AppId.'&secret='.$AppSecret;
           $arr=Api::geturl($getUrl);
           $arr=$arr['access_token'];
           $minutes=60;
           Cache::put("access_token",$arr, $minutes);
      }
      return $arr;
   }

   /*微信模版推送*/
   public static function template(){
       $token=Api::token();
       //return $token;
       $url="https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=$token";
       $arr['touser']="oRp9cxBPPJZLV6H1i5gxltHMZZbk";
       $arr['template_id']="PQLrI5SRbU-rRlx3kv9dlxeoKHQtmPeWCmuay0nTb_k";
       $arr['url']="https://www.baidu.com/";
       $arr['data']['first']['value']="您的账单已经生成";
       $arr['data']['first']['color']="#173177";
       $arr['data']['keyword1']['value']="888";
       $arr['data']['keyword1']['color']="#173177";
       $arr['data']['keyword2']['value']="99.00";
       $arr['data']['keyword2']['color']="#173177";
       $arr['data']['keyword3']['value']=date("Y:m:d H:i:s");
       $arr['data']['keyword3']['color']="#173177";
       $arr['data']['keyword4']['value']="刘汝涛";
       $arr['data']['keyword4']['color']="#173177";
       $arr['data']['keyword5']['value']="薄荷牙医";
       $arr['data']['keyword5']['color']="#173177";
       $arr['data']['remark']['value']="感谢您的使用";
       $arr['data']['remark']['color']="#173177";
       $arr=json_encode($arr);
       $data=Api::posturl($arr,$url);
       return $data;
   }

   public static function getSignaturePackage($url){
       //Cache::forget($url);
       Log::info("RRRRRRRRRRRRRRRRRRRRRRRRRRRRR");
       Log::info(Api::token());
        if(Cache::has($url)){
            $jsapiTicket = Cache::get($url);
        }
        else{
            $str='https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token='.Api::token().'&type=jsapi';
            $json=file_get_contents($str);
            $json=json_decode($json);
            $jsapiTicket=$json->ticket;
            $minutes=60;
            Cache::put($url,$jsapiTicket,$minutes);
        }
        //exit($jsapiTicket);
            $timestamp = time();
            $nonceStr = Api::createNonceStr();
            $string = "jsapi_ticket=$jsapiTicket&noncestr=$nonceStr&timestamp=$timestamp&url=$url";
            $signature = sha1($string);
            $signPackage = array(
              "appId"     => config('weixin.app.appid'),
              "nonceStr"  => $nonceStr,
              "timestamp" => $timestamp,
              "url"       => $url,
              "signature" => $signature,
              "rawString" => $string
            );
        return $signPackage;
    }

    public static function createNonceStr($length = 16){


     $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";


     $str = "";


     for ($i = 0; $i < $length; $i++) {


       $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);


     }


     return $str;


    }

    public static function wx_pay($body, $out_trade_no, $total_fee, $openid, $notify_url){
        $input['appid']=config('weixin.app.appid');
        $input['mch_id']=config('weixin.mch.mch_id');
        $input['nonce_str']=Api::randCode("16","1");
        $input['body']=$body;
        $input['out_trade_no']=$out_trade_no;
        $input['total_fee']=str_replace('.','',$total_fee);
        $input['spbill_create_ip']=$_SERVER['REMOTE_ADDR'];
        $input['notify_url']=$notify_url;
        $input['trade_type']="JSAPI";
        $input['openid']=$openid;
        $sign="appid=". $input['appid']."&body=".$input['body']."&mch_id=".$input['mch_id']."&nonce_str=".$input['nonce_str']."&notify_url=".$input['notify_url']."&openid=".$input['openid']."&out_trade_no=".$input['out_trade_no']."&spbill_create_ip=".$input['spbill_create_ip']."&total_fee=".$input['total_fee']."&trade_type=".$input['trade_type'];
        $key="03763595ca89decad9dd14d37115a366";
        $stringSignTemp=$sign."&key=".$key;
        $sign=md5($stringSignTemp);
        $sign =strtoupper($sign);
        $input['sign']=$sign;
        $xml=Api::ToXml($input);
        $url="https://api.mch.weixin.qq.com/pay/unifiedorder";
        Log::info('GGGGGGGGGGG$$$$$$$$$$$$$$$$$$$$$$$');
        Log::info('bbbbbbb',$input);
        $arr=Api::posturl($xml, $url);

        Log::info($arr);
        $wxobj=simplexml_load_string($arr, 'SimpleXMLElement', LIBXML_NOCDATA);
        Log::info($wxobj);
        if($wxobj->return_code=="SUCCESS"){
           return Api::wx_pay_control($wxobj->prepay_id,config('weixin.app.appid'));
        }
        else{
            return $wxobj;
        }
    }

    public static function wx_pay_control($prepay_id,$appid){
        $data['package']="prepay_id=".$prepay_id;
        $data['appId']=$appid;
        $data['timeStamp']=Api::gettime();
        $data['nonceStr']=Api::randCode("16","1");
        $data['signType']="MD5";
        $data['paySign']="appId=".$data['appId']."&nonceStr=".$data['nonceStr']."&package=".$data['package']."&signType=".$data['signType']."&timeStamp=".$data['timeStamp'];
        $key="03763595ca89decad9dd14d37115a366";
        $data['paySign']=$data['paySign']."&key=".$key;
        $data['paySign']=md5($data['paySign']);
        $data['paySign'] =strtoupper($data['paySign']);
        return $data;
    }
    public static function wx_userinfo($js_code){
        $appid=config('weixin.app.appid');
        $secret=config('weixin.app.appsecret');
        $url="https://api.weixin.qq.com/sns/jscode2session?appid=$appid&secret=$secret&js_code=$js_code&grant_type=authorization_code";
        $arr=Api::geturl($url);
        return $arr;
    }

    public static function api_notice_increment($url, $data){
        $ch = curl_init();
        $header = array("Accept-Charset: utf-8");
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $tmpInfo = curl_exec($ch);
        //     var_dump($tmpInfo);
        //    exit;
        if (curl_errno($ch)) {
          return false;
        }else{
          // var_dump($tmpInfo);
          return $tmpInfo;
        }
    }

    public static function getWxAppcodeUnlimit($scene, $page, $desc){
        $width=430;
        $arr='{"path":"'.$page.'","width":430'.',"scene":"'.$scene.'"}';
        $access_token=Api::token();
        $url="https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=$access_token";
        //$arr= json_encode($arr);
        $data= Api::api_notice_increment($url, $arr);
        file_put_contents("/var/www/html/storage/temp/qrcode_temp_$scene.png",$data);
        // $ret_qiniu = QiniuFuncs::uploadFileToQiniu("/var/www/html/storage/temp/qrcode_temp_$scene.png",'qrcodeadmin',$scene);
        // if(!$ret_qiniu)
        //   return 0;
        Storage::put('qrcode/'.$scene.'.png', $data);
        return 1;
   }
}
