<?php

return [
    'app' => [
       'appid' =>   env('WXAPP_APP_ID'),
       'appsecret' => env('WXAPP_APP_SECRET'),
       'notify_url' => 'https://wxif.suubook.com/api/weixin/payback'
    ],
    'mch' => [
       'mch_id' => env('WX_MCH_ID')
    ]
];
